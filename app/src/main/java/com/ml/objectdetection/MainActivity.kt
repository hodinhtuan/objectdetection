package com.ml.objectdetection

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayoutMediator
import com.ml.objectdetection.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val viewPager2 = binding.viewPager
        val tabLayout = binding.tabLayout
        // Set the viewPager2 adapter
        val adapter = PagerAdapter(supportFragmentManager, lifecycle)
        viewPager2.adapter = adapter

        //Enable/Disable swiping in ViewPager2
        viewPager2.isUserInputEnabled

        //TabLayoutMediator handles the task of generating page titles for the TabLayout
        TabLayoutMediator(tabLayout, viewPager2, false, false) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "analysis"
                }
                1 -> {
                    tab.text = "capture"
                }
            }
        }.attach()

        //menu/menu.xml
        binding.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.help -> {
                    //Handle icon press
                    openLabelList()
                    true
                }
                R.id.more -> {
                    openExtendFragment()
                    true
                }
                else -> false
            }
        }

    }

    fun openLabelList() {
        //Read .txt file from assets
        val fileName = "coco_dataset_labels.txt"
        val stringText = application.assets.open(fileName).bufferedReader().use { it.readText() }
        //Create Alert Dialog
        MaterialAlertDialogBuilder(this)
            .setTitle("List Of Labels In Dataset")
            .setMessage(stringText)
            .setPositiveButton("ok") { dialog, which ->
                // Respond to positive button press
            }
            .show()
    }

    fun openExtendFragment() {
        val extendFragment = ExtendFragment()
        extendFragment.show(supportFragmentManager, extendFragment.tag)
    }
}