package com.ml.objectdetection.cameraDetection

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.RectF
import android.media.Image
import android.util.Size
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import org.tensorflow.lite.DataType
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.Rot90Op

typealias CameraDetectorCallback = (image: List<DetectionObject>) -> Unit

class CameraDetector(
    private val yuvToRgbConverter: YuvToRgbConverter,
    private val loadModel: Interpreter,
    private val labels: List<String>,
    private val outputSize: Size,
    private val cameraDetectorCallback: CameraDetectorCallback

) : ImageAnalysis.Analyzer {

    private var rotateDegrees: Int = 0

    //ImageProcessor Architecture.
    /* The design of the ImageProcessor allowed the image manipulation operations to be defined up
    front and optimised during the build process. The ImageProcessor currently supports
    three basic preprocessing operations.*/
    private val tfImageProcessor by lazy {
        ImageProcessor.Builder()
            //Resize the image using Bilinear or Nearest neighbour.
            .add(
                ResizeOp(
                    IMG_WIDTH,
                    IMG_HEIGHT,
                    ResizeOp.ResizeMethod.NEAREST_NEIGHBOR
                )
            )
            .add(Rot90Op(-rotateDegrees / 90)) //Rotation counter-clockwise in 90 degree increments.
            .add(NormalizeOp(0f, 1f))
            .build()
    }

    /*Create a TensorImage object. This creates the tensor of the corresponding tensor
    type (uint8 in this case) that the TensorFlow Lite interpreter needs.*/
    private val tfImageBuffer = TensorImage(DataType.UINT8)

    private val outputDetect = arrayOf(Array(MAX_OBJECT_DETECTION) { FloatArray(4) })
    private val scores = arrayOf(FloatArray(MAX_OBJECT_DETECTION))
    private val labelOutput = arrayOf(FloatArray(MAX_OBJECT_DETECTION))

    private val outputBuffer = mapOf(
        0 to outputDetect,
        1 to labelOutput,
        2 to scores,
        3 to FloatArray(1)
    )


    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun analyze(image: ImageProxy) {
        if (image.image == null) return
        rotateDegrees = image.imageInfo.rotationDegrees
        val detectedObjectList = objectDetect(image.image!!)
        cameraDetectorCallback(detectedObjectList)
        image.close()
    }


    private fun objectDetect(image: Image): List<DetectionObject> {
        val bitmap = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
        //Convert to rgb
        yuvToRgbConverter.yuvToRgb(image, bitmap)
        tfImageBuffer.load(bitmap)
        val tensorImage = tfImageProcessor.process(tfImageBuffer)
        //Perform inference with a TensorFlow Lite model
        loadModel.runForMultipleInputsOutputs(arrayOf(tensorImage.buffer), outputBuffer)

        //Format the inference result and return it as a array list
        val listObjectDetected = arrayListOf<DetectionObject>()
        loopDetection@ for (i in 0 until MAX_OBJECT_DETECTION) {
            val score = scores[0][i]
            val label = labels[labelOutput[0][i].toInt()]
            val detectBoxes = RectF(
                outputDetect[0][i][1] * outputSize.width,
                outputDetect[0][i][0] * outputSize.height,
                outputDetect[0][i][3] * outputSize.width,
                outputDetect[0][i][2] * outputSize.height
            )

            if (score >= THRESHOLD) {
                listObjectDetected.add(
                    DetectionObject(
                        score = score,
                        label = label,
                        detectBoxes = detectBoxes
                    )
                )
            } else {
                break@loopDetection
            }


        }
        return listObjectDetected.take(4)
    }

    companion object {
        //Size of input and output model
        const val IMG_WIDTH = 300
        const val IMG_HEIGHT = 300

        //Maximum object detection
        const val MAX_OBJECT_DETECTION = 10

        //Recognition accuracy
        const val THRESHOLD = 0.5f
    }
}


data class DetectionObject(val detectBoxes: RectF, val label: String, val score: Float)
