package com.ml.objectdetection.cameraDetection

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.ml.objectdetection.databinding.Tab1Binding
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.nnapi.NnApiDelegate
import org.tensorflow.lite.support.common.FileUtil
import org.tensorflow.lite.support.model.Model
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.nio.MappedByteBuffer
import java.util.concurrent.Executors

class Tab1 : Fragment() {

    private val cameraExecutor = Executors.newSingleThreadExecutor()
    private val REQUIRE_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private val REQUESTS_CODE = 200
    private var boundingBox: BoundingBox? = null
    private lateinit var binding: Tab1Binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = Tab1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        boundingBox = BoundingBox(binding.surfaceView)
        // Request camera permissions
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(), REQUIRE_PERMISSIONS, REQUESTS_CODE
            )
        }
    }


    fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        //Used to bind the lifecycle of cameras to the lifecycle owner
        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            //Binding camera preview with the view
            val preview = Preview.Builder().build()
            preview.setSurfaceProvider(binding.cameraPreview.surfaceProvider)

            //Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            //Set up the image analysis, camera will process frames in real time
            val imageAnalysis = ImageAnalysis.Builder()
                .setTargetAspectRatio(binding.cameraPreview.display.rotation)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()

            //imageAnalysis.setAnalyzer analysis via an ImageProxy and draw bounding box on surfaceview
            imageAnalysis.setAnalyzer(
                cameraExecutor,
                CameraDetector(
                    yuvToRgbConverter,
                    loadModel,
                    label,
                    Size(binding.surfaceView.width, binding.surfaceView.height)
                ) { image: List<DetectionObject> ->
                    boundingBox?.onDrawBoundingBox(image)
                })

            try {
                //Unbind use cases before rebinding
                cameraProvider.unbindAll()
                //Bind use cases to lifecycle
                val camera = cameraProvider.bindToLifecycle(
                    this as LifecycleOwner, cameraSelector, preview, imageAnalysis
                )
            } catch (error: Exception) {
                Log.e(TAG, "Start camera failed", error)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    override fun onResume() {
        super.onResume()
        if (!allPermissionsGranted()) {
            requestPermission()
        } else {
            startCamera()
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(), REQUIRE_PERMISSIONS,
            REQUESTS_CODE
        )
    }

    private fun allPermissionsGranted() = REQUIRE_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUESTS_CODE) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(requireContext(), "Permission not granted", Toast.LENGTH_LONG).show()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private val yuvToRgbConverter: YuvToRgbConverter by lazy {
        YuvToRgbConverter(requireContext())
    }

    private val label: List<String> by lazy {
        loadLabel()
    }
    private val loadModel by lazy {
        Interpreter(
            FileUtil.loadMappedFile(requireContext(), MODEL_PATH),
            Interpreter.Options().addDelegate(
                NnApiDelegate()
            )
        )
    }

    //Load label from assets folder
    private fun loadLabel(): ArrayList<String> {

        val fileName: String = LABEL_PATH
        var label = ArrayList<String>()
        var inputStream: InputStream? = null
        try {
            inputStream = requireActivity().assets.open(fileName)
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            label = bufferedReader.readLines() as ArrayList<String>
        } catch (error: Exception) {
            Toast.makeText(requireContext(), "Failed to load label", Toast.LENGTH_LONG).show()
        } finally {
            inputStream?.close()
        }
        return label
    }

    companion object {
        private val TAG = Tab1::class.java.simpleName
        private const val MODEL_PATH = "ssd_mobilenet_v1.tflite"
        private const val LABEL_PATH = "coco_dataset_labels.txt"
    }
}