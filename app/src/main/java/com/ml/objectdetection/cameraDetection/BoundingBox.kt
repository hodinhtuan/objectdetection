package com.ml.objectdetection.cameraDetection

import android.graphics.*
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.ml.objectdetection.R

class BoundingBox(surfaceView: SurfaceView) : SurfaceView(surfaceView.context),
    SurfaceHolder.Callback {

    private var holders = surfaceView.holder
    private var paint = Paint()
    private var paintColor = Paint()
    private var opacityColor = context.resources.getColor(R.color.white_opacity_15)


    override fun surfaceCreated(holder: SurfaceHolder) {
        holders.setFormat(PixelFormat.TRANSPARENT)
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        //TODO("Not yet implemented")
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        //TODO("Not yet implemented")
    }

    fun onDrawBoundingBox(listObjectDetected: List<DetectionObject>) {
        val canvas: Canvas? = holders.lockCanvas()
        canvas?.drawColor(0, PorterDuff.Mode.CLEAR)
        listObjectDetected.mapIndexed { i, detectionObject ->

            //Draw the bounding box
            paint.apply {
                color = Color.RED
                style = Paint.Style.STROKE
                strokeWidth = 3f
                isAntiAlias = false
            }
            //rx, ry : used to round the corners
            //value : float
            canvas?.drawRoundRect(detectionObject.detectBoxes, 20f, 20f, paint)

            //Draw opacity color of the bounding box
            paintColor.apply {
                color = opacityColor
                style = Paint.Style.FILL
                isAntiAlias = false
            }

            canvas?.drawRoundRect(detectionObject.detectBoxes, 20f, 20f, paintColor)

            //Draw text of the bounding box
            paint.apply {
                style = Paint.Style.FILL
                isAntiAlias = true
                textSize = 40f
            }
            canvas?.drawText(
                detectionObject.label + ""  + "(%,.1f ".format(detectionObject.score * 100) + "%)",
                detectionObject.detectBoxes.left,
                detectionObject.detectBoxes.top - 5f,
                paint
            )
        }
        holders.unlockCanvasAndPost(canvas ?: return)
    }

    init {
        surfaceView.holder.addCallback(this)
        surfaceView.setZOrderOnTop(true)
    }
}

