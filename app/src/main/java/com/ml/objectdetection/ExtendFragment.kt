package com.ml.objectdetection

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ml.objectdetection.databinding.FragmentExtendBinding


class ExtendFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentExtendBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialogCustomStyle) //set border radius bottomsheet
    }


    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = View.inflate(context, R.layout.fragment_extend, null)
        binding = FragmentExtendBinding.bind(view)
        dialog.setContentView(view)


        binding.exitButton.setOnClickListener(View.OnClickListener {
            MaterialAlertDialogBuilder(requireActivity())
                .setTitle("Exit")
                .setMessage("Do you want to close this application?")
                .setNegativeButton("cancel") { dialog, which ->
                    //Dismiss alert dialog
                    dialog.dismiss()
                }
                .setPositiveButton("accept") { dialog, which ->
                    activity?.finishAndRemoveTask()
                    System.exit(0)
                }
                .show()
            dismiss()
        })
        return dialog
    }
}



