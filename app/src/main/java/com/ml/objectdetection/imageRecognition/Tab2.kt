package com.ml.objectdetection.imageRecognition

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.ml.objectdetection.R
import com.ml.objectdetection.databinding.Tab2Binding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.vision.detector.ObjectDetector
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min


class Tab2 : Fragment() {
    private lateinit var binding: Tab2Binding
    private lateinit var imagePath: String
    private var REQUEST_CODE: Int = 200
    private val SELECT_IMAGE = 250
    private var paint = Paint()
    private var paintOpacity = Paint()
    lateinit var bitmap: Bitmap


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = Tab2Binding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.cameraButton.setOnClickListener(View.OnClickListener {
            try {
                dispatchTakePictureIntent()
            } catch (e: ActivityNotFoundException) {
                Log.e(TAG, e.message.toString())
            }
        })
        //gallery button click action
        binding.galleryButton.setOnClickListener(View.OnClickListener {
            openPicture()
        })
    }

    //Open picture from gallery
    private fun openPicture() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(intent, SELECT_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE &&
            resultCode == Activity.RESULT_OK
        ) {
            setViewAndDetect(getCapturedImage())
        } else if (requestCode == SELECT_IMAGE &&
            resultCode == Activity.RESULT_OK
        ) {
            //Load a bitmap from uri
            var uri: Uri? = data?.data
            if (Build.VERSION.SDK_INT < 28) {
                //This method was deprecated in API level >= 29, use this for API < 28
                bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, uri)

                //Return of results
                setViewAndDetect(bitmap)
            } else {
                //Using this method for API level 29 or higher
                val source = ImageDecoder.createSource(requireContext().contentResolver, uri!!)
                bitmap = ImageDecoder.decodeBitmap(source)
                bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
                setViewAndDetect(bitmap)
            }
        }
    }

    private fun objectDetection(bitmap: Bitmap) {
        // Create TensorImage object from the Bitmap object
        val image = TensorImage.fromBitmap(bitmap)

        //Initialize
        val options = ObjectDetector.ObjectDetectorOptions.builder()
            .setMaxResults(MAX_DETECTION)
            .setScoreThreshold(THRESHOLD)
            .build()
        val objectDetector = ObjectDetector.createFromFileAndOptions(
            activity,
            MODEL_PATH,
            options
        )

        //Run inference
        val results = objectDetector.detect(image)

        //Parse the detection result and show it
        val resultToDisplay = results.map {
            // Get the top-1 category and craft the display text
            val category = it.categories.first()
            val text = "${category.label} (${category.score.times(100).toInt()}%)"
            // Create a data object to display the detection result
            DetectionObject(it.boundingBox, text)
        }

        // Draw the detection result on the bitmap and show it
        val imgResult = drawResult(bitmap, resultToDisplay)

        activity?.runOnUiThread {
            binding.imageView.setImageBitmap(imgResult)
        }
    }


    //Set image to imageview and call object detection
    private fun setViewAndDetect(bitmap: Bitmap) {
        // Display capture image
        binding.imageView.setImageBitmap(bitmap)
        //Hide the text view when bitmap is displaying
        binding.textView.visibility = View.INVISIBLE

        // Run ODT and display result
        // Note that we run this in the background thread to avoid blocking the app UI because
        // TFLite object detection is a synchronised process.
        lifecycleScope.launch(Dispatchers.Default) { objectDetection(bitmap) }
    }


    //Decodes and crops the captured image from camera.
    private fun getCapturedImage(): Bitmap {
        // Get the dimensions of the View
        val targetW: Int = binding.imageView.width
        val targetH: Int = binding.imageView.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            BitmapFactory.decodeFile(imagePath, this)

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = max(1, min(photoW / targetW, photoH / targetH))

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inMutable = true
        }
        val exifInterface = ExifInterface(imagePath)
        val orientation = exifInterface.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )

        val bitmap = BitmapFactory.decodeFile(imagePath, bmOptions)
        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> {
                rotateImage(bitmap, 90f)
            }
            ExifInterface.ORIENTATION_ROTATE_180 -> {
                rotateImage(bitmap, 180f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> {
                rotateImage(bitmap, 270f)
            }
            else -> {
                bitmap
            }
        }
    }

    //Decodes and crops the captured image from camera
    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }


    //Generates a temporary image file for the Camera app to write to
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            imagePath = absolutePath
        }
    }

    //Start the Camera app to take a photo
    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent

            activity?.let {
                takePictureIntent.resolveActivity(it.packageManager)?.also {
                    //Create the File where the photo should go
                    val photoFile: File? = try {
                        createImageFile()
                    } catch (e: IOException) {
                        Log.e(TAG, e.message.toString())
                        null
                    }
                    // Continue only if the File was successfully created
                    photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            requireActivity(),
                            "com.ml.objectdetection.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_CODE)
                    }
                }
            }
        }
    }

    //Draw a box around objects and show the objects name.
    private fun drawResult(
        bitmap: Bitmap,
        detectionResults: List<DetectionObject>
    ): Bitmap {
        val outputBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(outputBitmap)
        detectionResults.forEach {

            //Draw the bounding box
            paint.apply {

                color = Color.RED
                style = Paint.Style.STROKE
                strokeWidth = 3f
                isAntiAlias = false
            }
            //rx, ry : used to round the corners
            //value : float
            val detectBox = it.boundingBox
            canvas.drawRoundRect(detectBox, 20f, 20f, paint)


            //Draw opacity color of the bounding box
            paintOpacity.apply {
                color = ContextCompat.getColor(requireContext(), R.color.white_opacity_15)
                style = Paint.Style.FILL
                isAntiAlias = false
            }
            //rx, ry : used to round the corners
            //value : float
            val detectBoxes = it.boundingBox
            canvas.drawRoundRect(detectBoxes, 20f, 20f, paintOpacity)

            //Draw text of the bounding box
            paint.apply {
                style = Paint.Style.FILL
                isAntiAlias = true
                //textAlign = Paint.Align.LEFT
                textSize = 80f
            }
            val tagSize = Rect(0, 0, 0, 0)
            //paint.textSize = MAX_FONT_SIZE
            paint.getTextBounds(it.text, 0, it.text.length, tagSize)
            val fontSize: Float = paint.textSize * detectBox.width() / tagSize.width()

            // adjust the font size so texts are inside the bounding box
            if (fontSize < paint.textSize) paint.textSize = fontSize
            //canvas.drawText(it.text, detectBox.left, fontSize + detectBox.top - 5f, paint)
            canvas.drawText(it.text, detectBox.left + fontSize, detectBox.top - 5f, paint)


        }

        return outputBitmap
    }


    companion object {
        //Size of input and output model
        const val IMG_HEIGHT = 300
        const val IMG_WIDTH = 300

        //Maximum object detection
        const val MAX_DETECTION = 6

        //Recognition accuracy
        const val THRESHOLD = 0.3f
        private val TAG = Tab2::class.java.simpleName
        private const val MODEL_PATH = "ssd_mobilenet_v1.tflite"
        private const val LABEL_PATH = "coco_dataset_labels.txt"
    }
}

data class DetectionObject(val boundingBox: RectF, val text: String)