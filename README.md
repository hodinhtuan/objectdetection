# ObjectDetection

This is an example project intergrating Tensorflow Lite into Android App. The project includes detection object using cameraX, recognition image taken and SSD-MobileNet-V2-Quantized-COCO model.

## Getting started

![](https://gitlab.com/hodinhtuan/objectdetection/-/raw/main/video/objdtc1.mp4)
![](https://gitlab.com/hodinhtuan/objectdetection/-/raw/main/video/objdtc2.mp4)
![](https://gitlab.com/hodinhtuan/objectdetection/-/raw/main/video/objdtc3.mp4)

## Contact
If you have any ideas, please contact me via email hodinhtuan@hotmail.com.

## 
 
